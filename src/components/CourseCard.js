import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom';

export default function CourseCard({course}) {

	const {name, description, price, _id} = course;

	function enroll() {

	}


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
	         <Button className="bg-danger" >Remove</Button>
	    </Card.Body>
	</Card>
	)
}

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
